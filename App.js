import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

import Component from './Latihan/Component/Component';
import Toogle from './Latihan/Latihan13/index';
import YoutubeUI from './Tugas/Tugas12/App';
import Routes from './Tugas/Tugas13/Route';
import Latihan14 from './Latihan/Latihan14/App';
import Tugas14 from './Tugas/Tugas14/App';
import SkillScreen from './Tugas/Tugas14/SkillScreen';
import Latihan15 from './Latihan/Latihan15/Index';
import Tugas15 from './Tugas/Tugas15/index';
import LoginScreen from './Tugas/Tugas13_1/LoginScreen';
import RegisterScreen from './Tugas/Tugas13_1/RegisterScreen';
import AboutScreen from './Tugas/Tugas13_1/AboutScreen';
import TugasNavigation from './Tugas/TugasNavigation/index';
import Quiz3 from './Quiz3/index';

export default function App() {
  return (
    // <Component />
    // <YoutubeUI />
    // <Toogle />
    // <LoginScreen />
    // <RegisterScreen />
    // <AboutScreen />
    // <SkillScreen />
    // <Latihan14 />
    // <Tugas14 />
    // <Routes/>
    // <Latihan15 />
    // <Tugas15 />
    // <TugasNavigation />
    <View style={styles.container}>
      <StatusBar
        backgroundColor="#1c313a"
        barStyle="light-content"
      />
      <Quiz3 />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
});
