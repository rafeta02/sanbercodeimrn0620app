import React from 'react';
import { MaterialIcons } from '@expo/vector-icons';
import {
    StyleSheet,
    Text,
    View,
    TextInput,
    TouchableOpacity,
    Image
} from 'react-native';

export default class RegisterScreen extends React.Component {
    render() {
        return (
            <View style={styles.container}>
                <Image source={require('./images/logo.png')} style={{ width: 300, height: 100, marginTop: -100 }} />
                <Text style={{ marginTop: 15, fontSize: 25, fontWeight: 'bold' }}>Register</Text>
                <View style={styles.typeText}>
                    <MaterialIcons style={{ marginRight: 5 }} name="mail" size={24} color="black" />
                    <TextInput placeholder="Masukkan Username / Email" style={{ fontSize: 13 }}></TextInput>
                </View>
                <View style={styles.typeText}>
                    <MaterialIcons style={{ marginRight: 5 }} name="lock" size={24} color="black" />
                    <TextInput placeholder="Masukkan Password" style={{ fontSize: 13 }}></TextInput>
                </View>
                <View style={styles.typeText}>
                    <MaterialIcons style={{ marginRight: 5 }} name="lock" size={24} color="black" />
                    <TextInput placeholder="Masukkan Kembali Password" style={{ fontSize: 13 }}></TextInput>
                </View>
                <TouchableOpacity style={styles.button1}>
                    <Text style={{color: "white"}}>Masuk ?</Text>
                </TouchableOpacity>
                <View style={{ marginTop: 15 }}>
                <Text style={{color: '#3EC6FF'}}>Atau</Text>
                </View>
                <TouchableOpacity style={styles.button2}>
                    <Text style={{color: "white"}}>Daftar</Text>
                </TouchableOpacity>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center',
        display: 'flex',
    },
    text: {
        fontSize: 30, color: '#003366'
    },
    typeText: {
        marginTop: 25,
        width: 300,
        height: 30,
        paddingHorizontal: 10,
        borderBottomWidth: 1,
        borderColor: "#003366",
        flexDirection: "row"
    },
    button1: {
        marginTop: 40,
        height: 30,
        alignItems: 'center',
        backgroundColor: "#003366",
        paddingHorizontal: 25,
        paddingVertical: 5, 
        borderRadius: 200
    },
    button2: {
        marginTop: 15,
        height: 30,
        alignItems: 'center',
        backgroundColor: "#3EC6FF",
        paddingHorizontal: 25, 
        paddingVertical: 5, 
        borderRadius: 200
    }
});
