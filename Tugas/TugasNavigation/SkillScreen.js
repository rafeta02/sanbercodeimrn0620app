import React from 'react';
import Card from './components/Card.js'
import {
    StyleSheet,
    Text,
    View,
    FlatList,
    Image
} from 'react-native';
import skillData from './skillData.json'

export default class SkillScreen extends React.Component {
    render() {
        return (
            <View style={styles.container}>
                <Image source={require('./images/logo.png')} style={{ width: 180, height: 50, alignSelf: 'flex-end' }} />
                <View style={styles.userContainer}>
                    <Image source={require('./images/account.png')} style={{ width: 35, height: 35, borderRadius: 300, marginTop: 10 }} />
                    <View style={{ flexDirection: 'column', paddingLeft: 10 }}>
                        <Text style={{ color: "#003366" }}>Hai,</Text>
                        <Text style={{ color: "#003366" }}>Ramadhan Febri Utama</Text>
                    </View>
                </View>
                <Text style={{
                    alignSelf: 'flex-start',
                    color: "#003366",
                    borderBottomWidth: 4,
                    width: '90%',
                    borderBottomColor: "#3EC6FF",
                    marginLeft: 20,
                    marginBottom: 10,
                    fontSize: 36
                }}>SKILL</Text>
                <View style={styles.kategori}>
                    <View style={styles.listKategori}>
                        <Text style={{ fontSize: 11, color: "#003366" }}>Library / Framework</Text>
                    </View>
                    <View style={styles.listKategori}>
                        <Text style={{ fontSize: 11, color: "#003366" }}>Bahasa Pemrograman</Text>
                    </View>
                    <View style={styles.listKategori}>
                        <Text style={{ fontSize: 11, color: "#003366" }}>Teknologi</Text>
                    </View>
                </View>
                <FlatList
                    data={skillData.items}
                    renderItem={(skill) => <Card skill={skill} />}
                    keyExtractor={(item) => item.id.toString()}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'flex-start',
        display: 'flex',
        marginTop: 30
    },
    userContainer: {
        alignSelf: "flex-start",
        flexDirection: 'row',
        marginTop: 10,
        paddingLeft: 20
    },
    kategori: {
        flexDirection: "row",
        marginBottom: 10
    },
    listKategori: {
        backgroundColor: "#B4E9FF",
        marginHorizontal: 5,
        padding: 5,
        borderRadius: 10
    }
});
